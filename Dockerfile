FROM node:latest
LABEL Description="This image is used to build iquidus from github master"

RUN git clone https://github.com/iquidus/explorer.git /opt/iquidus-explorer

RUN cd /opt/iquidus-explorer && npm install --production

WORKDIR /opt/iquidus-explorer

CMD ["npm", "start"]